import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';



export default function OrderCard({orderProp}) {

	
	
	const { productName, quantity, price, subTotal, _id} = orderProp;

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Card.Subtitle>Subtotal:</Card.Subtitle>
                <Card.Text>Php {subTotal}</Card.Text>
                 <Button variant="primary" as={Link} to={`/users/${_id}`}>Details</Button>
                
            </Card.Body>
        </Card>
    )
}
