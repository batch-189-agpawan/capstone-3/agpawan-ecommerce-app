// import { useState, useEffect } from 'react';
import {Row, Col, Card, Button, Container}  from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function ProductCard({productProp}) {


    const { name, description, price, category, stock, _id } = productProp;
    // console.log(productProp)

    return(
        <Container>

        <Row className= " mt-3 mb-3">
            <Col xs={12} md={12}>
                <Card className="cardHighlight p-3 "style={{backgroundColor:'#CDC5B4'}}>
                    <Card.Body>
                    <Card.Title>{name}</Card.Title>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Title>PRICE</Card.Title>
                    <Card.Text>PHP {price}</Card.Text>                    
                    <Card.Title>Category</Card.Title>
                    <Card.Text>{category}</Card.Text>
                    <Card.Title>Stock</Card.Title>
                    <Card.Text>{stock}</Card.Text>
                    <Button variant="dark" as={Link} style={{backgroundColor:'#775144'}} to ={`/products/${_id}`}>Details</Button>
                    {/*<Link className="btn btn-primary"  to="/courseview">Details</Link>*/}
                    </Card.Body>
                </Card>
            </Col>
        </Row>
        </Container>

        )
}