import {Card} from 'react-bootstrap';




export default function UsersCard({usersProp}) {

	
	
	const { firstName, lastName, email, mobileNo, _id, isAdmin} = usersProp;
    
    return (
        <Card>
            <Card.Body>
                <Card.Title>{firstName} <span>{lastName}</span></Card.Title>
                <Card.Subtitle>{_id}</Card.Subtitle>
                <Card.Text>Email: {email}</Card.Text>
                <Card.Text>Mobile No: {mobileNo}</Card.Text>
                <Card.Text>Administrator: {String(isAdmin)}</Card.Text>
            </Card.Body>
        </Card>
    )
}


