
import { Link } from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../UserContext';
// pwede din ganito:
import { Navbar, Nav, Container, NavDropdown} from 'react-bootstrap';



export default function AppNavbar() {

  const {user} = useContext(UserContext);

  //State to store the user information stored in the login page
  // const [user, setUser] = useState(localStorage.getItem("email"));
  /*
    Syntax:
      localStorage.getItem("propertyName")
    getItem() method that returns value of a specified object item
  */


  console.log(user)

  return (

      <Navbar className="color-nav font-link" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/" className='font-link'><img src = {require('../images/logo.png')} height={30} width={70} alt="Arms Depot Logo"/>Arms Depot</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="justify-content-end" style={{ width: "100%" }}>
            <Nav.Link  as={Link} to="/" style={{color: '#CDC5B4'}}>Home</Nav.Link>
            <Nav.Link as={Link} to="/products" style={{color: '#CDC5B4'}}>Products</Nav.Link>
 

            {
              (user.id !== null) ?
              <>
              {
                (user.isAdmin === true) ?
                <>
            <NavDropdown title="Admin" id="basic-nav-dropdown" >              <NavDropdown.Item as={Link} to="/allproducts"style={{color: 'black'}}>Dashboard
              </NavDropdown.Item>
              <NavDropdown.Item as={Link} to="/createproduct" style={{color: 'black'}}>Add Product</NavDropdown.Item>                           
              <NavDropdown.Item as={Link} to="/logout"style={{color: 'black'}}>Log Out
              </NavDropdown.Item>
            </NavDropdown>
            </>

            :

            <>
            <NavDropdown title="User" id="basic-nav-dropdown" >
              <NavDropdown.Item as={Link} to="/Orders" style={{color: 'black'}}>My Cart</NavDropdown.Item>
              <NavDropdown.Item as={Link} to="/logout"style={{color: 'black'}}>Log Out
              </NavDropdown.Item>
            </NavDropdown>
            </>
          }
          </>

                  :

            <NavDropdown title="Get Started" id="basic-nav-dropdown" >
              <NavDropdown.Item as={Link} to="/login" style={{color: 'black'}}>Login</NavDropdown.Item>
              <NavDropdown.Item as={Link} to="/register"style={{color: 'black'}}>Register
              </NavDropdown.Item>
            </NavDropdown>
            }

          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
 
    )

}


