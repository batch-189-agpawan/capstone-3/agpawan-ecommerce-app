import { Link } from 'react-router-dom';
import {Button, Row, Col} from 'react-bootstrap';

export default function PageNotFound() {
		return(

			<Row>
				<Col className="p-5 font-link">
					<h1>Page Not Found</h1>
					<p>Go back to the <a href="/">homepage</a></p>
				</Col>
			</Row>
		)
}
